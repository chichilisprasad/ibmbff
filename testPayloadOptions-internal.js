const payload = {
    "apiURL": "http://localhost:6006/v1/",
    // "apiURL": "https://ey-rest-bff.mybluemix.net/v1/",
    "header": {
        'Content-Type': 'application/json',
        'sessionid': 'test-mocha-session',
        'tranid': 'test-mocha-tran',
        'appid': 'EYWCI'
    },
    "PNRSearch": {
        "recordLocator": "AZRJYT",
        "lastName": "eating",
        "searchType": "PNR"
    },
    "getSeatMap": { "flightOrdinal": 0, "requestUpsellCabin": true },
    "updateFQTV": {
        "ordinal": "1",
        "recordLocator": "YOZLSS",
        "fqtvInfo": {
            "number": "100014445",
            "plan": "9W"
        }
    },
    "addShoppingCart": {
        "ancillaryServiceDetails": [{
            "passengerOrdinal": "1",
            "flightOrdinal": "0",
            "ancillaryServiceType": "CHANGE_SEAT",
            "value": "10A",
            "price": "280",
            "currency": "AED"
        }]
    },
    "deleteShoppingCart": {
        "ancillaryServiceDetails": [{
            "passengerOrdinal": "1",
            "flightOrdinal": "0",
            "ancillaryServiceType": "CHANGE_SEAT",
            "value": "10A"
        }]
    },
    "updateRegDoc": {
        "regDocInfo": [{
            "documentData": {
                "lastName": "TEST",
                "firstName": "TEST",
                "infant": "false",
                "dateOfBirth": "19900101",
                "nationalityCode": "US",
                "gender": "M",
                "documentNumber": "12345678A",
                "issuingCountryCode": "US",
                "dateOfExpiry": "20200101",
                "documentType": "PASSPORT",
                "countryOfResidence": "US",
                "knownTravelerNumber": "99867665",
                "ordinal": "1"
            }
            ,
            "emergencyContact": {
                "contactName": "CHARLES",
                "phoneNumber": "9011386104",
                "relationship": "WORK",
                "required": "false",
                "countryCode": "USA"
            },

            "visaDocumentData": {
                "countryCode": "USA",
                "documentNumber": "12345",
                "nationalityCode": "USA",
                "issuingCity": "USA",
                "issuingDate": "2010-10-10"
            },
            "residencyData": {
                "countryCode": "IND",
                "country": "IND",
                "countryName": "IND",
                "zip": "411027",
                "state": "MAHARASTRA",
                "street": "WAKAD",
                "city": "PUNE"
            },
            "destinationData": {
                "country": "USA",
                "zip": "53202",
                "state": "US",
                "street": "89987 MAIN",
                "city": "CALIFOR"
            }
            ,
            "dateOfReturn": ""
        }],
        "ordinal": "1"
    }
};
const options = {
    "apiURL": payload.apiURL,
    "PNRSearch": {
        method: 'POST',
        url: payload.apiURL + 'PNRSearch',
        headers: payload.header,
        body: payload.PNRSearch,
        json: true
    },
    "seatMap": {
        method: 'POST',
        url: payload.apiURL + 'getSeatMap',
        headers: payload.header,
        body: payload.getSeatMap,
        json: true
    },
    "updateFQTV": {
        method: 'POST',
        url: payload.apiURL + 'updateFQTV',
        headers: payload.header,
        body: payload.updateFQTV,
        json: true
    },
    "getShoppingCart": {
        method: 'GET',
        url: payload.apiURL + 'shoppingCart',
        headers: payload.header
    },
    "addShoppingCart": {
        method: 'POST',
        url: payload.apiURL + 'shoppingCart',
        headers: payload.header,
        body: payload.addShoppingCart,
        json: true
    },
    "deleteShoppingCart": {
        method: 'POST',
        url: payload.apiURL + 'shoppingCart/remove',
        headers: payload.header,
        body: payload.deleteShoppingCart,
        json: true
    },
    "updateRegDoc": {
        method: 'POST',
        url: payload.apiURL + 'updateRegDoc',
        headers: payload.header,
        body: payload.updateRegDoc,
        json: true
    }

}
module.exports = options;