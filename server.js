const http = require('http');
//exported from app.js
const app = require('./app');

const port = process.env.port || 3000;
const server = http.createServer(app);
server.listen(port);
