const expect = require('chai').expect;
const request = require('request');
//const payloadOptions = require('../../testPayloadOptions');
const payloadOptions = require('../../testPayloadOptions-internal');

describe('API Automation Test for ' + payloadOptions.apiURL, () => {

    it('POST - PNRSearch', (done) => {
        request(payloadOptions.PNRSearch, (error, response, body) => {
            //console.log(body);
            expect(response.statusCode).to.equal(200);
            done();
        });
    });

    it('POST - seatMap', (done) => {
        request(payloadOptions.seatMap, (error, response, body) => {
            expect(response.statusCode).to.equal(200);
            done();
        });
    });

    it('POST - updateFQTV', (done) => {
        request(payloadOptions.updateFQTV, (error, response, body) => {
            expect(response.statusCode).to.equal(200);
            done();
        });
    });

    it('GET - ShoppingCart', (done) => {
        request(payloadOptions.getShoppingCart, (error, response, body) => {
            expect(response.statusCode).to.equal(200);
            done();
        });
    });

    it('POST - ShoppingCart', (done) => {
        request(payloadOptions.addShoppingCart, (error, response, body) => {
            expect(response.statusCode).to.equal(200);
            done();
        });
    });

    it('POSt - shoppingCart/remove', (done) => {
        request(payloadOptions.deleteShoppingCart, (error, response, body) => {
            expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it('POST - updateRegDoc', (done) => {
        request(payloadOptions.updateRegDoc, (error, response, body) => {
            console.log(body);
            expect(response.statusCode).to.equal(200);
            done();
        });
    });
    
});
