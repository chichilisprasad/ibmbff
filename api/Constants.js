var APIConstants = {
    "identifyPNRurl": "https://ey-rest.mybluemix.net/api/identifyPNR",
    "registerClientUrl": "http://ey-rest-internal.mybluemix.net/api/registerClient",
    "getSeatMap": "https://ey-rest.mybluemix.net/api/getSeatMap",
    "sessionEndUrl": "http://ey-rest.mybluemix.net/api/endSession",
    "updateFQTV": "https://ey-rest.mybluemix.net/api/updateFQTV",
    "shoppingCartUrl": "http://ey-rest.mybluemix.net/api/shoppingcart",   
    "sessionEndBody": {
        "reason": "CHECKIN_COMPLETE", "lastScreenName": "CheckinComplete.html"
    },    
    "error": {
        "headerNotFound": {
            "errorCode": "Headers are not passed",
            "root": "BFF",
            "errorMessage": "Headers are not passed",
            "customMessages": []
        },
        "badRequest": {
            "errorCode": "Bad request",
            "root": "BFF",
            "errorMessage": "Bad request",
            "customMessages": []
        },
        "CTSConnectionError": {
            "errorCode": "CTS_Connection error",
            "root": "BFF",
            "errorMessage": '',
            "customMessages": []
        },
        "internalServerError": {
            "errorCode": "Internal Server Error",
            "root": "BFF",
            "errorMessage": 'Internal Server Error',
            "customMessages": []
        },
        "unauthorizedError": {
            "errorCode": "Unauthorized User",
            "root": "BFF",
            "errorMessage": "Unauthorized User / Session expired",
            "customMessages": []
        }
    }
}
module.exports = APIConstants;