let getPnrResponse = (response) => {
    try {
        let idPNRResponse = {};

        idPNRResponse.atleastOnePaxCheckedIn = false;
        idPNRResponse.boardingPassEnabledForMobile = true;
        idPNRResponse.boardingPassEnabledForWeb = true;
        idPNRResponse.bookingLanguageCode = 'en';
        idPNRResponse.recordLocator = response.PNRIDResponse.checkinOptions.pnrInfo.recordLocator;
        idPNRResponse.gdspnr = response.PNRIDResponse.checkinOptions.pnrInfo.recordLocatorGDS;
        idPNRResponse.emailAddress = response.PNRIDResponse.checkinOptions.pnrInfo.emailAddress;
        //idPNRResponse.paidSeat = response.PNRIDResponse.checkinOptions.passengers[0].flights[0].seat?response.PNRIDResponse.checkinOptions.passengers[0].flights[0].seat.paidSeat:false;        
        //Build Open flights
        let openFlights = {};
        let legs = [];
        if (response.PNRIDResponse.checkinOptions.flights) {
            legs = response.PNRIDResponse.checkinOptions.flights;
            if (legs.length > 0) {
                openFlights.checkinOpenIn = 6;
                openFlights.checkinOpenInFormat = 'days';
                openFlights.stops = legs.length - 1;
                openFlights.tripDepartureDate = legs[0].departure.scheduledTimeLocal.toString().replace("AM", " AM").replace("PM", " PM");
                openFlights.tripDestinationCode = legs[legs.length - 1].arrival.airport.code;
                openFlights.tripOriginCode = legs[0].departure.airport.code;
            }
        }


        let flights = [];
        let i = 0;
        //Build Legs
        for (let leg of legs) {
            i = i + 1;
            //console.log('legs', legs[i]);
            let flight = {};

            flight.aircraftType = leg.aircraftType;
            flight.arrivalAirportCode = leg.arrival.airport.code;
            flight.arrivalDate = leg.arrival.scheduledTimeLocal.toString().split(' ')[0];
            flight.arrivalDateChangeIndicator = leg.arrivalNextDay && leg.arrivalNextDay > 0;
            flight.arrivalScheduledTime = leg.arrival.scheduledTimeLocal.toString().split(' ')[1];
            flight.arrivalTerminal = leg.arrival.airport.terminal;
            flight.gate = leg.arrival.gate;
            flight.boardTime = leg.departure.boardTimeLocal.toString().replace("AM", " AM").replace("PM", " PM");
            flight.cabinClassCode = response.PNRIDResponse.checkinOptions.passengers[0].flights.filter(x => x.flightOrdinal == leg.ordinal)[0].cabinClass;
            flight.carrierCode = leg.carrierCode;
            flight.changeSeatIndicator = leg.seatChangeEnabled;
            flight.departureAirportCode = leg.departure.airport.code;
            flight.departureDate = leg.departure.scheduledTimeLocal.toString().split(' ')[0];
            flight.departureScheduledTime = leg.departure.scheduledTimeLocal.toString().split(' ')[1];            
            flight.departureTerminal = leg.departure.airport.terminal;
            console.log('depltterminal:'+flight.departureTerminal);
            flight.flightDurationhrs = `${Math.floor(leg.duration / 60)}`;
            flight.flightDurationmins = `${Math.floor(leg.duration % 60)}`;
            //flight.flightDuration = `${Math.floor(leg.duration / 60)} hrs ${Math.floor(leg.duration % 60)} mins`;
            flight.flightStatus = leg.status;
            flight.isDelayed = leg.status != "ON_TIME" ? true : false;
            flight.isCancelled = leg.status == "CANCELLED" ? true : false;
            flight.gate = leg.departure.gate.replace('GATE','');
            console.log('gate:'+flight.gate);
            flight.hasAllPaxCheckedIn = false;
            if (legs[i]) {
                if (legs[i].departure.boardTime) {
                    const tomeToMins = 1000 * 60;
                    const timeToHours = tomeToMins * 60;
                    //console.log('leg.arrival.scheduledTime###', legs[i].departure.scheduledTime, 'leg.arrival.scheduledTime###',leg.arrival.scheduledTime)
                    flight.layoverDuration = legs[i].departure.scheduledTime - leg.arrival.scheduledTime;

                    if (flight.layoverDuration) {
                        //console.log('flight.layoverDuration',flight.layoverDuration);
                        flight.layoverDuration = `${Math.floor(flight.layoverDuration / timeToHours)} hrs ${(flight.layoverDuration / tomeToMins) % 60} mins`
                    }
                }
            }
            else {
                flight.layoverDuration = "";
            }
            flight.layoverAirportCode = leg.arrival.airport.code;
            flight.marketingCarrierCode = leg.operatingCarrierCode;
            flight.marketingCarrierNumber = leg.operatingNumber;
            flight.operatingCarrierCode = leg.operatingCarrierCode;
            flight.operatingCarrierNumber = leg.operatingNumber;
            flight.ordinal = leg.ordinal;
            flight.outbound = leg.outbound;
            flight.returnSegment = leg.returnSegment;
            flight.seatChangeEnabled = leg.seatChangeEnabled;
            flight.segmentType = leg.segmentType;
            flight.checkinStatus = leg.checkinStatus;
            flights.push(flight);
        }
        openFlights.flights = flights;

        //Build Passengers
        let passengers = [];
        let paxs = response.PNRIDResponse.checkinOptions.passengers;
        for (let pax of paxs) {
            let passenger = {};
            passenger.associatedWithInfant = pax.infant != null;
            passenger.baggagePieces = 1;
            passenger.baggageUnit = 'KG';
            passenger.baggageWeight = 32;
            passenger.checkinSeq = false;
            passenger.dateOfBirth = pax.dateOfBirth;
            passenger.eticketNumber = pax.eticketNumber;
            passenger.eticketReceiptPrintAllowed = pax.eticketReceiptPrintAllowed;
            passenger.firstName = pax.firstName;
            passenger.fqtvcarrierCode = pax.fqtvcarrierCode;
            passenger.fqtvfirstName = pax.fqtvfirstName;
            passenger.fqtvlastName = pax.fqtvlastName;
            passenger.fqtvUpdateAllowed = pax.fqtvUpdateAllowed;
            passenger.gender = pax.gender;
            passenger.infant = pax.infant;
            passenger.lastName = pax.lastName;
            passenger.ordinal = pax.ordinal;
            passenger.premiumPassenger = pax.premiumPassenger;
            passenger.regDocCompleteInd = pax.regDocStatus;
            passenger.seatChangeAllowed = pax.seatChangeAllowed;
            passenger.title = pax.title;
            passenger.totalWeight = pax.totalWeight;
            passenger.type = pax.type;
            passenger.regDocInfo = pax.regDocInfo;
            passenger.checkinStatus = pax.checkinStatus;
            let fqtvInfo = {};
            if (pax.fqtvInfo) {
                fqtvInfo.fqtvAirline = pax.fqtvInfo.plan;
                fqtvInfo.fqtvNumber = pax.fqtvInfo.number;
                fqtvInfo.fqtvTier = pax.fqtvInfo.tier;
                passenger.fqtvInfo = fqtvInfo;
            }
            //Build passenger flights
            let paxFlights = [];

            for (let leg of pax.flights) {
                let flight = {};
                let openFlight = openFlights.flights.filter(x => x.ordinal === leg.flightOrdinal)[0];
                if (openFlight == null) continue;
                flight.aircraftType = leg.aircraftType;
                flight.allowCheckin = true;
                flight.allowDownloadBP = true;
                flight.allowEmailBP = true;
                flight.allowPrintBP = true;
                flight.allowWatsappBP = true;
                flight.departure = { airport: { code: openFlight.departureAirportCode } };
                flight.arrival = { airport: { code: openFlight.arrivalAirportCode } };
                flight.bookingClass = leg.bookingClass;
                flight.cabinType = leg.cabinType;
                flight.carrierCode = openFlight.carrierCode;
                flight.flightNumber = openFlight.operatingCarrierCode + " " + openFlight.operatingCarrierNumber;
                flight.ordinal = leg.ordinal;
                flight.flightOrdinal = leg.flightOrdinal;
                flight.outbound = openFlight.outbound;
                flight.returnSegment = openFlight.returnSegment;
                flight.seatChangeEnabled = openFlight.seatChangeEnabled;
                flight.seat = leg.seat ? leg.seat.number : null;
                flight.checkinStatus = leg.checkinStatus;
                flight.paidSeat = leg.seat?leg.seat.paidSeat:null;
                paxFlights.push(flight);
            }

            passenger.flights = paxFlights;
            passengers.push(passenger);
        }

        openFlights.passengers = passengers;
        idPNRResponse.openFlights = openFlights;

        //Build future flights
        let futureFlights = [];
        let resFutureFlights = response.PNRIDResponse.futureFlights;
        if (resFutureFlights)
            for (let leg of resFutureFlights) {
                let flight = {};

                flight.aircraftType = leg.aircraftType;
                flight.arrivalAirportCode = leg.arrival.airport.code;
                flight.arrivalDate = leg.arrival.scheduledTimeLocal.toString().split(' ')[0];
                flight.arrivalDateChangeIndicator = leg.arrivalNextDay && leg.arrivalNextDay > 0;
                flight.arrivalScheduledTime = leg.arrival.scheduledTimeLocal.toString().split(' ')[1];
                flight.cabinClassCode = leg.passengers[0].bookingClass;
                flight.changeSeatIndicator = leg.seatChangeEnabled;
                flight.checkinOpenIn = Math.floor(leg.minutesToDepart / (24 * 60));
                flight.checkinOpenInFormat = "days";
                flight.departureAirportCode = leg.departure.airport.code;
                flight.departureDate = leg.departure.scheduledTimeLocal.toString().split(' ')[0];
                flight.departureScheduledTime = leg.departure.scheduledTimeLocal.toString().split(' ')[1];
                flight.flightDurationhrs = `${Math.floor(leg.duration / 60)}`;
                flight.flightDurationmins = `${Math.floor(leg.duration % 60)}`;
                //flight.flightDuration = `${Math.floor(leg.duration / 60)} hrs ${Math.floor(leg.duration % 60)} mins`;
                flight.flightStatus = leg.status;
                flight.isDelayed = leg.status != "ON_TIME" ? true : false;
                flight.isCancelled = leg.status == "CANCELLED" ? true : false;
                flight.hasAllPaxCheckedIn = false;
                flight.marketingCarrierCode = leg.carrierCode;
                flight.marketingCarrierNumber = leg.number;
                flight.operatingCarrierCode = leg.operatingCarrierCode;
                flight.operatingCarrierNumber = leg.operatingNumber;
                flight.ordinal = leg.ordinal;
                flight.outbound = leg.outbound;
                flight.returnSegment = leg.returnSegment;
                flight.seatChangeEnabled = leg.seatChangeEnabled;
                flight.segmentType = leg.segmentType;

                let passengers = [];

                for (let pass of leg.passengers) {
                    let passenger = {};
                    let pax = openFlights.passengers.filter(x => x.ordinal == pass.passengerOrdinal)[0];
                    passenger.associatedWithInfant = pax.infant != null;
                    // passenger.baggagePieces = 1;
                    // passenger.baggageUnit = 'KG';
                    // passenger.baggageWeight = 32;
                    passenger.checkinSeq = false;
                    passenger.dateOfBirth = pax.dateOfBirth;
                    passenger.eticketNumber = pax.eticketNumber;
                    passenger.eticketReceiptPrintAllowed = pax.eticketReceiptPrintAllowed;
                    passenger.firstName = pax.firstName;
                    passenger.fqtvcarrierCode = pax.fqtvcarrierCode;
                    passenger.fqtvfirstName = pax.fqtvfirstName;
                    passenger.fqtvlastName = pax.fqtvlastName;
                    passenger.fqtvUpdateAllowed = pax.fqtvUpdateAllowed;
                    passenger.gender = pax.gender;
                    passenger.infant = pax.infant;
                    passenger.lastName = pax.lastName;
                    passenger.ordinal = pax.ordinal;
                    passenger.premiumPassenger = pax.premiumPassenger;
                    passenger.regDocCompleteInd = pax.regDocStatus;
                    passenger.seatChangeAllowed = pax.seatChangeAllowed;
                    passenger.title = pax.title;
                    passenger.totalWeight = pax.totalWeight;
                    passenger.type = pax.type;
                    passenger.regDocInfo = pax.regDocInfo;
                    passenger.seat = pass.seat ? pass.seat.number : null;

                    let fqtvInfo = {};
                    fqtvInfo.fqtvAirline = pass.fqtvInfo ? pass.fqtvInfo.plan : null;
                    fqtvInfo.fqtvNumber = pass.fqtvInfo ? pass.fqtvInfo.number : null;
                    fqtvInfo.fqtvTier = pass.fqtvInfo ? pass.fqtvInfo.tier : null;
                    fqtvInfo.fqtvCode = pass.fqtvInfo ? pass.fqtvInfo.code : null;
                    passenger.fqtvInfo = fqtvInfo;

                    passengers.push(passenger);
                }
                flight.passengers = passengers;
                futureFlights.push(flight);
            }
        idPNRResponse.futureFlights = futureFlights;

        return idPNRResponse;
    } catch (error) {
        return response;
    }
}

module.exports = getPnrResponse;