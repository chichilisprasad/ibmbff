const aisleText = 'AISLE';
const windowText = 'WINDOW';
const rearText = 'REAR';
const centreSectionText = 'CENTER_SECTION';
const rightSectionText = 'RIGHT_SIDE_SECTION';
const leftSectionText = 'LEFT_SIDE_SECTION';
const exitRowText = 'ExitRow';
const noSeatText = 'NoSeatAtThisLocation';
const basinetSeatText = 'SEAT_WITH_BASSINET_FACILITY';
const toBeOfferedLastText = 'SEAT_TO_BE_LEFT_VACANT_OFFERED_LAST';
const rearFacingText = 'RearFacingSeat';
const legSpaceSeatText = 'LEG_SPACE_SEAT';
const preferredSeatText = 'PreferredSeat/PreferentialSeat';

let getSeatMapResponse = (response, requestBody) => {
    //let ordinal = requestBody.flightOrdinal;
    let seatMapNode = response.enhancedSeatMapRS.seatMap[0];
    let flightNode = seatMapNode ? seatMapNode.flight : null;
    let cabinNode = seatMapNode.cabin[0];
    let seatMapResponse = {};

    populateCommonSeatMapProperties(seatMapNode, flightNode, cabinNode, seatMapResponse)
    try {
        let ordinal = requestBody.flightOrdinal;
        let seatMapNode = response.enhancedSeatMapRS.seatMap[0];
        let flightNode = seatMapNode.flight;
        let cabinNode = seatMapNode.cabin[0];
        let seatMapResponse = {};

        populateCommonSeatMapProperties(seatMapNode, flightNode, cabinNode, seatMapResponse)
        let wingStart = cabinNode.wing != null ? parseInt(cabinNode.wing.firstRow) : 0;
        let wingEnd = cabinNode.wing != null ? parseInt(cabinNode.wing.lastRow) : 0;
        seatMapResponse.wingstrart = wingStart;
        seatMapResponse.wingend = wingEnd;
        populateSeatMapRows(seatMapNode, flightNode, cabinNode, seatMapResponse, ordinal);
        return seatMapResponse;
    } catch (error) {
        return response;
    }

}

let populateCommonSeatMapProperties = (seatMapNode, flightNode, cabinNode, seatMapResponse) => {
    try {
        seatMapResponse.ChangeOfGaugeInd = seatMapNode.changeOfGaugeInd;
        seatMapResponse.Equipment = seatMapNode.equipment;
        seatMapResponse.Destination = flightNode.destination;
        seatMapResponse.Origin = flightNode.origin;
        seatMapResponse.DepartureDate = flightNode.departureDate;
        seatMapResponse.OperatingCarrierFlightNumber = `${flightNode.operating.carrier} ${flightNode.operating.text}`;
        seatMapResponse.OperatingCarrierCode = flightNode.operating.carrier;
        seatMapResponse.MarketingCarrierFlightNumber = `${flightNode.marketing.carrier} ${flightNode.marketing.text}`;
        seatMapResponse.MarketingCarrierCode = flightNode.marketing.carrier;
        seatMapResponse.CabinClass = cabinNode.cabinClass.rbd;
        seatMapResponse.Deck = cabinNode.classLocation;
        seatMapResponse.Facilities = [];
        seatMapResponse.Prices = []
        if (cabinNode.cabinClass.classLocation) {
            seatMapResponse.CabinClass = cabinNode.cabinClass.classLocation;
        }
        seatMapResponse.rows = [];
    } catch (error) {

    }

}

let populateSeatMapRows = (seatMapNode, flightNode, cabinNode, seatMapResponse, ordinal) => {
    let wingStart = cabinNode.wing != null ? parseInt(cabinNode.wing.firstRow) : 0;
    let wingEnd = cabinNode.wing != null ? parseInt(cabinNode.wing.lastRow) : 0;
    //console.log(wingStart,wingEnd)
    for (let row of cabinNode.row) {
        let currentRow = {};
        currentRow.RowNumber = parseInt(row.rowNumber);
        currentRow.IsWing = currentRow.RowNumber >= wingStart && currentRow.RowNumber <= wingEnd;
        currentRow.IsSeatRow = true;
        currentRow.IsFacilityRow = false;
        currentRow.IsExitRow = row.type.filter(t => t.value == 'EXIT_ROW').length > 0;
        populateSeatForRows(cabinNode, row, currentRow, seatMapResponse, ordinal);
        seatMapResponse.rows.push(currentRow);
        populateFacilities(row, seatMapResponse)

    }
}

let populateFacilities = (row, seatMapResponse) => {
    if (row.rowFacility) {
        let rowFacilities = []
        if (isArray(row.rowFacility)) {
            rowFacilities = row.rowFacility;
        }
        else {
            rowFacilities.push(row.rowFacility);
        }

        for (let facility of rowFacilities) {
            if (facility.location == rearText) {
                seatMapResponse.rows.push(getFacilityRow(facility, seatMapResponse));
            }
            else {
                seatMapResponse.rows.splice(seatMapResponse.rows.length - 1, 0, getFacilityRow(facility, seatMapResponse));

            }
        }
    }
}

let populateSeatForRows = (cabinNode, row, currentRow, seatMapResponse, ordinal) => {
    currentRow.seats = [];
    let column = [];
    let followsPath = false;
    //////
    if (cabinNode.column.length != row.seat.length) {
        let colIndex = 0;
        for (let col of cabinNode.column) {
            if (row.seat && row.seat.filter(t => t.number == col.column).length == 0) {
                row.seat.splice(
                    colIndex,
                    0,
                    {
                        "number": col.column,
                        "occupation": [],
                        "location": [],
                        "limitations": [],
                        "facilities": [],
                        "price": [],
                        "passengerEntitlementAndPrice": [],
                        "bilateral": null,
                        "amenityRef": [],
                        "blockCode": null,
                        "occupiedInd": false,
                        "inoperativeInd": false,
                        "premiumInd": false,
                        "chargeableInd": false,
                        "exitRowInd": false,
                        "restrictedReclineInd": false,
                        "noInfantInd": false
                    });
            }
            colIndex++;
        }
    }

    let aisleColumns = cabinNode.column.filter(t => t.characteristics == aisleText).map(t => t.column);


    for (let seat of row.seat) {


        let currentSeat = {};
        currentSeat.Facilities = [];
        currentSeat.FlightOrdinal = ordinal;
        currentSeat.Exists = seat.location.filter(t => t.detail.value == "NO_SEAT_AT_THIS_LOCATION").length == 0;
        currentSeat.BlockCode = seat.blockCode ? seat.blockCode : '';
        currentSeat.SeatNumber = seat.number;
        currentSeat.RowNumber = row.rowNumber;
        currentSeat.IsAisle = aisleColumns.includes(currentSeat.SeatNumber);
        currentSeat.IsWindow = seat.location.filter(t => t.detail.value == "WINDOW").length > 0;
        currentSeat.IsPaid = seat.chargeableInd;
        currentSeat.IsPremium = seat.premiumInd;
        currentSeat.IsOccupied = seat.occupiedInd;
        currentSeat.IsExitRow = seat.exitRowInd;
        currentSeat.IsInoperative = seat.inoperativeInd;
        currentSeat.IsNotForInfant = seat.noInfantInd;
        currentSeat.IsRestrictedRecline = seat.restrictedReclineInd;

        currentSeat.IsRearFacing = seat.limitations && seat.limitations.filter(t => t.detail.value == 'REAR_FACING_SEAT').length > 0;
        currentSeat.ToBeOfferedLast = seat.limitations && seat.limitations.filter(t => t.detail.value == 'SEAT_TO_BE_LEFT_VACANT_OFFERED_LAST').length > 0;

        currentSeat.IsExtraLegRoom = false;
        currentSeat.IsPreferential = false;
        currentSeat.HasBassinet = false;


        let seatFacilities = [];
        if (seat.facilities && isArray(seat.facilities)) {
            seatFacilities = seat.facilities;
        }
        else if (seat.facilities) {
            seatFacilities.push(seat.facilities);
        }

        for (let facility of seatFacilities) {
            if (facility.detail.value == basinetSeatText) {
                currentSeat.Facilities.push(basinetSeatText);
                currentSeat.HasBassinet = true;
            }
            if (facility.detail.value == legSpaceSeatText) {
                currentSeat.Facilities.push(legSpaceSeatText);
                currentSeat.IsExtraLegRoom = true;

            }
            if (facility.detail.value == preferredSeatText) {
                currentSeat.Facilities.push(preferredSeatText);
                currentSeat.IsPreferential = true;
            }
        }

        if (seat.price[0] && seat.price[0].totalAmount) {
            let price = Number(seat.price[0].totalAmount.value);
            if (price > 0) {

                currentSeat.Currency = seat.price[0].totalAmount.currencyCode;
                currentSeat.Price = seat.price[0].totalAmount.value;
                if (seat.price[0].totalAmount.decimalPlaces) {
                    currentSeat.PriceDecimal = seat.price[0].totalAmount.decimalPlaces.toString();
                }
                currentSeat.taxes = seat.price[0].taxes;
                if (!seatMapResponse.Prices.includes(seat.price[0].totalAmount.value)) {
                    seatMapResponse.Prices.push(seat.price[0].totalAmount.value);
                }
            }
            else {
                currentSeat.Currency = seat.price[0].totalAmount.currencyCode;
                currentSeat.Price = seat.price[0].totalAmount.value;
                currentSeat.IsPaid = false;
            }
        }


        ////
        column.push(currentSeat);

        if (!followsPath && currentSeat.IsAisle) {
            followsPath = true;
            currentRow.seats.push(column);
            column = [];
        }
        else {
            followsPath = false
        }
    }
    if (column.length) {
        currentRow.seats.push(column);
    }
}

getFacilityRow = (facility, seatMapResponse) => {
    let facilityRow = {};
    facilityRow.IsSeatRow = false;
    facilityRow.IsFacilityRow = true;
    facilityRow.Location = facility.location;
    facilityRow.IsExitRow = facility.type == exitRowText;

    let facilities = [];
    if (facility.facility && isArray(facility.facility)) {
        facilities = facility.facility;
    }
    else {
        facilities.push(facility.facility);
    }

    let centerSection = facilities.filter(t => t.location.value == centreSectionText);

    if (centerSection.length > 0) {
        facilityRow.HasCenter = true;
        facilityRow.Center = centerSection[0].characteristics.extension ? centerSection[0].characteristics.extension : centerSection[0].characteristics.value;
        if (!seatMapResponse.Facilities.includes(facilityRow.Center)) {
            seatMapResponse.Facilities.push(facilityRow.Center);
        }
    }
    else {
        facilityRow.HasCenter = false;
    }
    let rightSection = facilities.filter(t => t.location.value == rightSectionText);
    if (rightSection.length > 0) {
        facilityRow.HasRight = true;
        facilityRow.Right = rightSection[0].characteristics.extension ? rightSection[0].characteristics.extension : rightSection[0].characteristics.value;
        if (!seatMapResponse.Facilities.includes(facilityRow.Right)) {
            seatMapResponse.Facilities.push(facilityRow.Right);
        }
    }
    else {
        facilityRow.HasRightSection = false
    }
    let leftSection = facilities.filter(t => t.location.value == leftSectionText);
    if (leftSection.length > 0) {
        facilityRow.HasLeft = true;
        facilityRow.Left = leftSection[0].characteristics.extension ? leftSection[0].characteristics.extension : leftSection[0].characteristics.value;
        if (!seatMapResponse.Facilities.includes(facilityRow.Left)) {
            seatMapResponse.Facilities.push(facilityRow.Left);
        }
    }
    else {
        facilityRow.HasLeft = false;
    }
    return facilityRow;
}


let isArray = (data) => {
    return (Object.prototype.toString.call(data) === "[object Array]");
}

module.exports = getSeatMapResponse;