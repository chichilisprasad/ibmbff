require('dotenv/config');
const redis = require('redis');
const redisClient = redis.createClient(JSON.parse(process.env.REDISCONNECTION_STRING));
redisClient.auth(process.env.REDIS_PASSWORD, function (err, reply) {
    console.log('Redis AUTH', err || reply);
});
redisClient.on('ready', function () {
    console.log("Redis is ready");
});
redisClient.on('error', function (e) {
    console.log("Error in Redis", e);
});
function getRedisClient(){
    return redisClient;
}

module.exports = redisClient; //getRedisClient(); 