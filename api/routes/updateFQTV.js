//Update FQTV APIS
require('dotenv/config');
const express = require('express');
const updateFQTVrouter = express.Router();
const bodyParser = require("body-parser");
var apiConstants = require('../Constants');
const request = require('request');
const redisClient = require('../routes/RedisClient');
  
updateFQTVrouter.get('/', (req, res, next) => {
    redisClient.get('sample', function (err, transactionId) {
        console.log('id', err, transactionId);
    });
    res.statusCode = 200;
    res.json({
        message: 'i am in updateFQTV client get'
    });
});
updateFQTVrouter.post('/', (req, res, next) => {
    try {
        //If headers are passed
        if (req.headers['sessionid'] && req.headers['tranid'] && req.headers['appid']) {
            //Check the request body 
            if (req.body.hasOwnProperty('ordinal') && req.body.hasOwnProperty('recordLocator') && req.body.hasOwnProperty('fqtvInfo')) {
                //Calling redis to get the CTS transactionID
                console.log("heder" + req.headers['sessionid']);
                redisClient.get(req.headers['sessionid'], function (err, transactionId) {
                    console.log(err, transactionId);
                    //if transaction id is available
                    if (transactionId) {
                        const updateFQTVOptions = {
                            method: 'POST',
                            url: process.env.UPDATEFQTV,
                            headers:
                            {
                                'Content-Type': 'application/json',
                                'cts-transaction-id': transactionId,
                                'sessionId': req.headers['sessionid'],
                                'APPId': req.headers['appid']
                            },
                            body: req.body,
                            json: true
                        }
                        request(updateFQTVOptions, (updateFQTVError, updateFQTVResponse, updateFQTVBody) => {
                            if (updateFQTVError) {
                                let ctsError = apiConstants.error.CTSConnectionError;
                                ctsError.errorMessage = updateFQTVError.errorMessage;
                                res.status(500).send(ctsError);
                            } else {
                                if (updateFQTVBody.errorResponse) {
                                    if (updateFQTVBody.errorResponse.errorCode === 'PAX_NOT_FOUND') {
                                        res.status(200).send({ 'errorCode': 'CE003' });
                                    } else if (updateFQTVBody.errorResponse.errorCode === 'SABRE') {
                                        res.status(200).send({ 'errorCode': 'CE004' });
                                    } else if (updateFQTVBody.errorResponse.errorCode === 'TRANSACTION_FAILED') {
                                        res.status(200).send({ 'errorCode': 'CE005' });
                                    } 
                                    else {
                                        res.status(200).send({ 'errorCode': 'CE005' });
                                    }
                                }else{
                                    res.status(updateFQTVResponse.statusCode).send(updateFQTVBody);
                                }
                            }
                        });
                    }
                    //If transaction id does not found
                    else {
                        res.status(401).send({ 'errorCode': 'CE009' });
                    }
                });
            }
            //
            else {
                res.status(400).send({ 'errorCode': 'CE009' });
            }
        }
        //If headers are not passed
        else {
            res.status(400).send({ 'errorCode': 'CE009' });
        }

    } catch (error) {
        console.log(error);
        res.status(500).send({ 'errorCode': 'CE009' });
    }
});
module.exports = updateFQTVrouter;