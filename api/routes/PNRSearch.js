//PNR Search APIS
 require('dotenv/config');
const express = require('express');
const request = require('request');
const redisClient = require('../routes/RedisClient');
const apiConstants = require('../Constants');
const getPnrResponse = require('../Model/pnrresponseconversion');
const router = express.Router();

router.get('/', (req, res, next) => {
    res.statusCode = 200;
    console.log(process.env.SECRECT_MESSAGE);
    res.json({
        message: 'i am in register client get'
    });

});
router.post('/', (req, res, next) => {
    try {
        //console.log('req.headers',req.headers);
        if (req.headers['sessionid'] && req.headers['tranid'] && req.headers['appid']) {
            if (req.body.searchType && req.body.lastName) {
                let deviceDetails = {
                    "deviceID": "PUNEREST",
                    "clientType": "WEB",
                    "requestConfig": true
                }
                let searchRequest = { "lastName": req.body.lastName };
                if (req.body.searchType === 'PNR' && req.body.recordLocator) {
                    searchRequest = {
                        "recordLocator": req.body.recordLocator,
                        "lastName": req.body.lastName,
                        "searchType": "PNR"
                    }
                } else if (req.body.searchType === 'ticketNumber' && req.body.ticketNumber) {
                    searchRequest = {
                        "ticketNumber": req.body.ticketNumber,
                        "lastName": req.body.lastName,
                        "searchType": "TKT"
                    }
                }
                else if (req.body.searchType === 'FFY' && req.body.fqtvNumber) {
                    searchRequest = {
                        "fqtvNumber": req.body.fqtvNumber,
                        "fqtvCode": "EY",
                        "lastName": req.body.lastName,
                        "searchType": "FFP"
                    }
                }
                // console.log(searchRequest);
                if (searchRequest.searchType) {
                    const options = {
                        method: 'POST',
                        url: process.env.REGISTERCLIENT,
                        headers:
                        {
                            'Content-Type': 'application/json',
                            'sessionId': req.headers['sessionid'],
                            'APPId': req.headers['appid']
                        },
                        body: deviceDetails,
                        json: true
                    }
                    console.log(options);
                    console.log("Info:" + req.headers['sessionid'] + req.headers['tranid'] +
                        " component identifier:BFF instance identifier:PNRSearch Payload:" + JSON.stringify(searchRequest) + " direction:request log position:Request received from API Timestamp" + Date.now());
                    request(options, (error, resp, body) => {
                        if (error) {
                            res.status(200).send({ 'errorCode': 'FE011' });
                        } else {
                            if (body.errorResponse) {
                                if(body.errorResponse.errorMessage === 'TRANSACTION_FAILED'){
                                    res.status(500).send({ 'errorCode': 'CE002' });
                                }else{
                                    res.status(500).send({ 'errorCode': 'CE009' });
                                }
                            } else {
                                //console.log(resp.headers);
                                const transactionId = resp.headers['cts-transaction-id'];
                                console.log('transactionId ', transactionId);
                                if (transactionId) {

                                    const searchOptions = {
                                        method: 'POST',
                                        url: process.env.IDENTIFYPNR,
                                        headers:
                                        {
                                            'Content-Type': 'application/json',
                                            'cts-transaction-id': transactionId,
                                            'sessionId': req.headers['sessionid'],
                                            'APPId': req.headers['appid']
                                        },
                                        body: searchRequest,
                                        json: true
                                    }
                                    console.log("Info:" + req.headers['sessionid'] + req.headers['tranid'] + transactionId +
                                        " component identifier:BFF instance identifier:PNRSearch Payload:" + JSON.stringify(searchRequest) + " direction:request log position:Request received from API Timestamp" + Date.now());
                                    request(searchOptions, (searchError, searchResponse, searchBody) => {
                                        // console.log(searchError,searchResponse,searchBody);
                                        if (searchError) {
                                            res.status(200).send({ 'errorCode': 'FE011' });
                                        } else {
                                            if (searchBody.errorResponse) {
                                                if (searchBody.errorResponse.errorCode === 'INVALID_REQUEST') {
                                                    res.status(200).send({ 'errorCode': 'FE001' });
                                                } else if (searchBody.errorResponse.errorCode === 'TRANSACTION_IN_PROGRESS') {
                                                    res.status(200).send({ 'errorCode': 'FE002' });
                                                } else if (searchBody.errorResponse.errorCode === 'DATA_MISSING') {
                                                    res.status(200).send({ 'errorCode': 'FE003' });
                                                } else if (searchBody.errorResponse.errorCode === 'SEE_AGENT_GENERAL') {
                                                    res.status(200).send({ 'errorCode': 'FE004' });
                                                }
                                                else if (searchBody.errorResponse.errorCode === 'PAX_NOT_FOUND') {
                                                    res.status(200).send({ 'errorCode': 'FE005' });
                                                } else if (searchBody.errorResponse.errorCode === 'INVALID_DEPARTURE_LOCATION') {
                                                    res.status(200).send({ 'errorCode': 'FE006' });
                                                }
                                                else if (searchBody.errorResponse.errorCode === 'CHECKIN_FAILED') {
                                                    res.status(200).send({ 'errorCode': 'FE007' });
                                                }
                                                else if (searchBody.errorResponse.errorCode === 'RESTRICTED_FLIGHT') {
                                                    res.status(200).send({ 'errorCode': 'FE008' });
                                                }
                                                else if (searchBody.errorResponse.errorCode === 'TRANSACTION_FAILED') {
                                                    res.status(200).send({ 'errorCode': 'FE009' });
                                                }
                                                else {
                                                    res.status(200).send({ 'errorCode': 'FE009' });
                                                }

                                                const sessionEndOptions = {
                                                    method: 'POST',
                                                    url: process.env.SESSIONEND,
                                                    headers:
                                                    {
                                                        'Content-Type': 'application/json',
                                                        'cts-transaction-id': transactionId,
                                                        'sessionId': req.headers['sessionid'],
                                                        'APPId': req.headers['appid']
                                                    },
                                                    body: apiConstants.sessionEndBody,
                                                    json: true
                                                }
                                                request(sessionEndOptions, (sessionEndError, sessionEndResponse, sessionEndBody) => { });
                                            } else {
                                                redisClient.set(req.headers['sessionid'], transactionId, function (err, msg) {
                                                    res.send(getPnrResponse(searchResponse.body));
                                                });
                                            }
                                        }
                                    });
                                } else {
                                    res.status(200).send({ 'errorCode': 'FE010' });
                                }
                            }
                        }
                    });
                } else {
                    res.status(400).send({ 'errorCode': 'FE001' });
                }
            } else {
                res.status(400).send({ 'errorCode': 'FE001' });
            }
        } else {
            res.status(400).send({ 'errorCode': 'FE001' });
        }
    } catch (error) {
        console.log(error);
        res.status(500).send(apiConstants.error.internalServerError);
    }
});

module.exports = router;