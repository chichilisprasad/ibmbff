require('dotenv/config');
const express = require('express');
const router = express.Router();
const apiConstants = require('../Constants');
const request = require('request');
const redisClient = require('../routes/RedisClient');


router.get('/', (req, res, next) => {
    try {
        if (req.headers['sessionid'] && req.headers['tranid'] && req.headers['appid']) {
            redisClient.get(req.headers['sessionid'], function (err, transactionId) {
                console.log(err, transactionId);
                if (transactionId) {
                    const shoppingCartOptions = {
                        method: 'GET',
                        url: process.env.SHOPPINGCART,
                        headers:
                        {
                            'Content-Type': 'application/json',
                            'cts-transaction-id': transactionId,
                            'sessionId': req.headers['sessionid'],
                            'APPId': req.headers['appid']
                        }
                    }
                    request(shoppingCartOptions, (shoppingCartError, shoppingCartResponse, shoppingCartBody) => {
                        if (shoppingCartError) {
                            // let ctsError = apiConstants.error.CTSConnectionError;
                            // ctsError.errorMessage = shoppingCartError.errorMessage;
                            // res.status(500).send(ctsError);

                            res.status(500).send({ 'errorCode': 'SE009' });
                        } else {
                            if (shoppingCartBody.errorResponse) {
                                if (shoppingCartBody.errorResponse.errorCode === 'DATA_MISSING') {
                                    res.status(200).send({ 'errorCode': 'SE001' });
                                } else if (shoppingCartBody.errorResponse.errorCode === 'ORDINAL_OUT_OF_RANGE') {
                                    res.status(200).send({ 'errorCode': 'SE002' });
                                } else if (shoppingCartBody.errorResponse.errorCode === 'TRANSACTION_FAILED') {
                                    res.status(200).send({ 'errorCode': 'SE009' });
                                }
                                else {
                                    res.status(500).send({ 'errorCode': 'SE009' });
                                }
                            }else{
                                res.status(shoppingCartResponse.statusCode).send(shoppingCartBody);
                            }
                            
                        }
                    });
                } else {
                    res.status(401).send({ 'errorCode': 'SE001' });
                }
            });

        } else {
            res.status(400).send({ 'errorCode': 'SE001' });
        }
    } catch (error) {
        console.log(error);
        res.status(500).send(apiConstants.error.internalServerError);
    }
});
router.post('/', (req, res, next) => {
    try {
        if (req.headers['sessionid'] && req.headers['tranid'] && req.headers['appid']) {
            if (req.body.ancillaryServiceDetails && req.body.ancillaryServiceDetails.length > 0) {
                redisClient.get(req.headers['sessionid'], function (err, transactionId) {
                    console.log(err, transactionId);
                    if (transactionId) {
                        const shoppingCartOptions = {
                            method: 'POST',
                            url: process.env.SHOPPINGCART,
                            headers:
                            {
                                'Content-Type': 'application/json',
                                'cts-transaction-id': transactionId,
                                'sessionId': req.headers['sessionid'],
                                'APPId': req.headers['appid']
                            },
                            body: req.body,
                            json: true
                        };
                        request(shoppingCartOptions, (shoppingCartError, shoppingCartResponse, shoppingCartBody) => {
                            if (shoppingCartError) {
                                let ctsError = apiConstants.error.CTSConnectionError;
                                ctsError.errorMessage = shoppingCartError.errorMessage;
                                //res.status(500).send(ctsError);
                                res.status(500).send({ 'errorCode': 'SE009' });
                            } else {
                                if (shoppingCartBody.errorResponse) {
                                    if (shoppingCartBody.errorResponse.errorCode === 'DATA_MISSING') {
                                        res.status(200).send({ 'errorCode': 'SE001' });
                                    } else if (shoppingCartBody.errorResponse.errorCode === 'ORDINAL_OUT_OF_RANGE') {
                                        res.status(200).send({ 'errorCode': 'SE002' });
                                    } else if (shoppingCartBody.errorResponse.errorCode === 'TRANSACTION_FAILED') {
                                        res.status(200).send({ 'errorCode': 'SE009' });
                                    }
                                    else {
                                        res.status(200).send({ 'errorCode': 'SE009' });
                                    }
                                }else{
                                    res.status(shoppingCartResponse.statusCode).send(shoppingCartBody);
                                } 
                            }
                        });
                    } else {
                        res.status(401).send({ 'errorCode': 'SE001' });
                    }
                });
            } else {
                res.status(400).send({ 'errorCode': 'SE001' });
            }
        } else {
            res.status(400).send({ 'errorCode': 'SE001' });
        }
    } catch (error) {
        console.log(error);
        res.status(500).send({ 'errorCode': 'SE009' });
    }
});
router.post('/remove', (req, res, next) => {
    try {
        if (req.headers['sessionid'] && req.headers['tranid'] && req.headers['appid']) {
            if (req.body.ancillaryServiceDetails && req.body.ancillaryServiceDetails.length > 0) {
                redisClient.get(req.headers['sessionid'], function (err, transactionId) {
                    console.log(err, transactionId);
                    if (transactionId) {
                        const shoppingCartOptions = {
                            method: 'post',
                            url: process.env.REMOEVESHOPPINGCART,
                            headers:
                            {
                                'Content-Type': 'application/json',
                                'cts-transaction-id': transactionId,
                                'sessionId': req.headers['sessionid'],
                                'APPId': req.headers['appid']
                            },
                            body: req.body,
                            json: true
                        }
                        request(shoppingCartOptions, (shoppingCartError, shoppingCartResponse, shoppingCartBody) => {
                            if (shoppingCartError) {
                                let ctsError = apiConstants.error.CTSConnectionError;
                                ctsError.errorMessage = shoppingCartError.errorMessage;
                                res.status(500).send(ctsError);
                            } else {
                                if (shoppingCartBody.errorResponse) {
                                    if (shoppingCartBody.errorResponse.errorCode === 'DATA_MISSING') {
                                        res.status(200).send({ 'errorCode': 'SE003' });
                                    } else if (shoppingCartBody.errorResponse.errorCode === 'ORDINAL_OUT_OF_RANGE') {
                                        res.status(200).send({ 'errorCode': 'SE002' });
                                    } else if (shoppingCartBody.errorResponse.errorCode === 'TRANSACTION_FAILED') {
                                        res.status(200).send({ 'errorCode': 'SE009' });
                                    }
                                    else {
                                        res.status(200).send({ 'errorCode': 'SE009' });
                                    }
                                }else{
                                    res.status(shoppingCartResponse.statusCode).send(shoppingCartBody);
                                }
                            }
                        });
                    } else {
                        res.status(401).send({ 'errorCode': 'SE003' });
                    }
                });
            } else {
                res.status(400).send({ 'errorCode': 'SE003' });
            }
        } else {
            res.status(400).send({ 'errorCode': 'SE003' });
        }
    } catch (error) {
        console.log(error);
        res.status(500).send({ 'errorCode': 'SE009' });
    }
});

module.exports = router;