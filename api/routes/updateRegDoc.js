require('dotenv/config');
const express = require('express');
const request = require('request');
const apiConstants = require('../Constants');
const redisClient = require('../routes/RedisClient');
const router = express.Router();

router.post('/', (req, res, next) => {
    try {
        if (req.headers['sessionid'] && req.headers['tranid'] && req.headers['appid']) {
            const isValidRequest = req.body.hasOwnProperty('regDocInfo')  && req.body.regDocInfo.length > 0 ;
                //&& req.body.regDocInfo[0].hasOwnProperty('documentData') && req.body.regDocInfo[0].hasOwnProperty('emergencyContact')
                //&& req.body.regDocInfo[0].hasOwnProperty('visaDocumentData') && req.body.regDocInfo[0].hasOwnProperty('residencyData')
                //&& req.body.regDocInfo[0].hasOwnProperty('destinationData') && req.body.regDocInfo[0].hasOwnProperty('dateOfReturn')

            // console.log(isValidRequest, req.body);
            if (isValidRequest) {
                redisClient.get(req.headers['sessionid'], function (err, transactionId) {
                    console.log(err, transactionId);
                    //console.log("Info:" +req.headers['sessionid']  + req.headers['tranid'] + transactionId +
                    //" component identifier:BFF instance identifier:updateRegDoc Payload:" + JSON.stringify(req.body) + " direction:Response log position:Request received from API Timestamp"+Date.now());
                    if (transactionId) {
                        const updateRegDocOptions = {
                            method: 'POST',
                            url: process.env.UPDATEREGDOC,
                            headers:
                            {
                                'Content-Type': 'application/json',
                                'cts-transaction-id': transactionId,
                                'sessionId': req.headers['sessionid'],
                                'APPId': req.headers['appid']
                            },
                            body: req.body,
                            json: true
                        }
                        request(updateRegDocOptions, (updateRegDocError, updateRegDocResponse, updateRegDocBody) => {
                            if (updateRegDocError) {
                                let ctsError = apiConstants.error.CTSConnectionError;
                                ctsError.errorMessage = updateRegDocError.errorMessage;
                                // res.status(500).send(ctsError);
                            } else {
                                if (updateRegDocBody.errorResponse) {
                                    if (updateRegDocBody.errorResponse.errorCode === 'DATA_MISSING') {
                                        res.status(200).send({ 'errorCode': 'LE004' });
                                    } else if (updateRegDocBody.errorResponse.errorCode === 'TRAVELDOC_EXPIRED') {
                                        res.status(200).send({ 'errorCode': 'LE005' });
                                    } else if (updateRegDocBody.errorResponse.errorCode === 'REG_DOC_UPDATE_ERROR') {
                                        res.status(200).send({ 'errorCode': 'LE006' });
                                    }
                                    else {
                                        res.status(200).send({ 'errorCode': 'LE009' });
                                    }
                                } else {
                                    res.status(updateRegDocResponse.statusCode).send(updateRegDocBody);
                                }

                            }
                        });
                    } else {
                        res.status(401).send({ 'errorCode': 'LE004' });
                    }
                });
            } else {
                res.status(400).send({ 'errorCode': 'LE004' });
            }
        } else {
            res.status(400).send({ 'errorCode': 'LE004' });
        }
    } catch (error) {
        res.status(500).send({ 'errorCode': 'LE009' });
    }

});

module.exports = router;