// Seat MAP APIS
require('dotenv/config');
const express = require('express');
const router = express.Router();
const apiConstants = require('../Constants');
const request = require('request');
const redisClient = require('../routes/RedisClient');
const getSeatMapResponse = require('../Model/getSeatMapResponseConversion');

router.get('/', (req, res, next) => {
});
router.post('/', (req, res, next) => {
    try {
        if (req.headers['sessionid'] && req.headers['tranid'] && req.headers['appid']) {
            if (req.body.hasOwnProperty('flightOrdinal') && req.body.hasOwnProperty('requestUpsellCabin')) {
                redisClient.get(req.headers['sessionid'], function (err, transactionId) {
                    console.log(err, transactionId);
                     console.log("Info:" +req.headers['sessionid']  + req.headers['tranid'] + transactionId +
                                        " component identifier:BFF instance identifier:seatMap Payload:" + JSON.stringify(req.body) + " direction:Response log position:Request received from API Timestamp"+Date.now());
                    if (transactionId) {
                        const seatMapOptions = {
                            method: 'POST',
                            url: process.env.GETSEATMAP,
                            headers:
                            {
                                'Content-Type': 'application/json',
                                'cts-transaction-id': transactionId,
                                'sessionId': req.headers['sessionid'],
                                'APPId': req.headers['appid']
                            },
                            body: {
                                'flightOrdinal': req.body.flightOrdinal,
                                'requestUpsellCabin': req.body.requestUpsellCabin
                            },
                            json: true
                        }
                        request(seatMapOptions, (seatMapError, seatMapResponse, seatMapBody) => {
                            if (seatMapError) {
                                let ctsError = apiConstants.error.CTSConnectionError;
                                ctsError.errorMessage = seatMapError.errorMessage;
                                res.status(500).send(ctsError);
                            } else {
                                console.log("Info:" +req.headers['sessionid']  + req.headers['tranid'] + transactionId +
                                        " component identifier:BFF instance identifier:seatMap Payload:" + JSON.stringify(req.body) +"seatMap response payload "+JSON.stringify(seatMapResponse)+ " direction:Response log position:Response received from API Timestamp"+Date.now());
                                // if(seatMapResponse.enhancedSeatMapRS && seatMapResponse.enhancedSeatMapRS.applicationResults && seatMapResponse.enhancedSeatMapRS.applicationResults.error && seatMapResponse.enhancedSeatMapRS.applicationResults.error.length>0){
                                //     res.status(seatMapResponse.statusCode).send(seatMapBody);
                                // }else{
                                //     res.status(seatMapResponse.statusCode).send(getSeatMapResponse(seatMapBody,req));
                                // }
                                res.status(seatMapResponse.statusCode).send(getSeatMapResponse(seatMapBody,req));                                                               
                            }
                        });
                    } else {
                        res.status(401).send(apiConstants.error.unauthorizedError);
                    }
                });
            } else {
                res.status(400).send(apiConstants.error.badRequest);
            }
        } else {
            res.status(400).send(apiConstants.error.headerNotFound);
        }
    } catch (error) {
        res.status(500).send(apiConstants.error.internalServerError);
    }

});
module.exports = router;