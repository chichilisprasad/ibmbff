const express = require('express');
const cfenv = require('cfenv');
const app = express();
const bodyParser = require("body-parser");
const cors = require('cors');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./api/swagger.json');
const PNRSearch = require('./api/routes/PNRSearch');
const updateFQTV = require('./api/routes/updateFQTV');
const seatMap = require('./api/routes/seatMap');
const shoppingCart = require('./api/routes/shoppingCart');
const updateRegDoc = require('./api/routes/updateRegDoc');

var appEnv = cfenv.getAppEnv();
app.use(cors()); // allow cross origin request
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    next();
});

const swaggerOptions = {
    swaggerOptions: {
    validatorUrl : null
  }
}
app.use('/v1/api/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, swaggerOptions));
app.use('/v1/PNRSearch',PNRSearch);
app.use('/v1/getSeatMap',seatMap);
app.use('/v1/updateFQTV',updateFQTV);
app.use('/v1/shoppingCart',shoppingCart);
app.use('/v1/updateRegDoc',updateRegDoc);

module.exports =app;
app.listen(appEnv.port, '0.0.0.0', function () {
    console.log("server starting on " + appEnv.url);    
 });